<?php

namespace App\Http\Resources;

use App\Models\ProductAttributes;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            "id" => $this->id,
            "nombre" => $this->nombre,
            "precio" => $this->precio,
            "cantidad" => $this->cantidad,
            "atributos" => $this->productAttributes()->get()
        ];
    }
}
