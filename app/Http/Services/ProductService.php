<?php
namespace App\Http\Services;

use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Models\ProductAttributes;


class ProductService {

    public function index() : ProductResource
    {
        return  new ProductResource(Product::all());
    }
 
    public function store(array $productData): ProductResource
    {

        $atributos = ProductAttributes::create($productData["atributos"]);
        $data = $productData;
        $data["atributos"] = $atributos;
        $product = Product::create($data);
 
        return new ProductResource($product);
    }
 
    public function update(array $productData, Product $product): ProductResource
    {
        $product->update($productData);

        return new ProductResource($product);
    }

    public function delete(Product $product)
    {
        $product->delete();
    }
}

