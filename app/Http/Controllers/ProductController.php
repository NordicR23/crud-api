<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Http\Services\ProductService;

class ProductController extends Controller
{
    public function __construct(private ProductService $productService)
    {
        
    }

    public function index(){
        $products = $this->productService->index();
        return response($products->json(),200);
    }

    public function store(ProductRequest $request){
        $product = $this->productService->store($request->all());
        
        return response([
            "message" => "Producto almacenado correctamente",
            "data" => $product      
        ],200);
    }

    public function update(ProductRequest $request){
        $product = $this->productService->update($request->all(), Product::find($request->id));
        
        return response([
            "message" => "Producto actualizado correctamente",
            "data" => $product      
        ],200);
    }

    public function delete(ProductRequest $request){
        $this->productService->delete(Product::find($request->id));

        return response([
            "message" => "Producto borrado exitosamente", 
        ],200);
    }
}
