<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductAttributes extends Model
{
    protected $fillable = [
        'color',
        'forma',
        'tamano',
    ];

    public $timestamps = false;

    use HasFactory;
}
