<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre',
        'precio',
        'cantidad',
        'id_attributes'
    ];
    public $timestamps = false;

    public function productAttributes(){
        return $this->hasOne(ProductAttributes::class);
    }
}
