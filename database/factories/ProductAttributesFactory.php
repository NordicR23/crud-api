<?php

namespace Database\Factories;

use App\Models\ProductAttributes;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ProductAttributes>
 */
class ProductAttributesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'color' => fake()->colorName(),
            'forma' => fake()->name(),
            'tamano' => fake()->randomNumber(),
        ];
    }
}
