<?php

namespace Database\Factories;

use App\Models\ProductAttributes;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            "nombre" => fake()->name(),
            "tipo" => fake()->name(),
            "precio" => fake()->randomNumber(3),
            "cantidad" => fake()->randomNumber(1),
            "attributes_id" => ProductAttributes::factory(),
        ];
    }
}
